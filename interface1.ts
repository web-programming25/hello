interface Rectangle1{
    width: number;
    height: number;
}

interface ColoredRectangle extends Rectangle1{
    color: string
}

const rectangle1: Rectangle1={
    width: 20,
    height: 10
}
console.log(rectangle1)

const coloredRectangle: ColoredRectangle={
    width:20,
    height:10,
    color:"red"
}

console.log(coloredRectangle);